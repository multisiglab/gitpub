#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/multi_index.hpp>

#include <string>
#include <vector>

using namespace eosio;
using namespace std;

class gitpub : public eosio::contract {
	private:
		/// @abi table storages i64
		struct storage {
			account_name worker;
			string ip;
			string peerid;

			storage() {}
			storage( account_name worker, string ip, string peerid ) : worker(worker), ip(ip), peerid(peerid) {}

			auto primary_key() const { return worker; }

			EOSLIB_SERIALIZE( storage, (worker)(ip)(peerid) )
		};

		typedef eosio::multi_index< N(storages), storage > storages;

		struct host {
			account_name worker;
			string peerid;
			bool status;
			string hostlink;

			host() {}
			host( account_name worker, string peerid ) : worker(worker), peerid(peerid) {
				status = 0;
				hostlink = "";
			}

			EOSLIB_SERIALIZE( host, (worker)(peerid)(status)(hostlink) )
		};

		/// @abi table repos i64
		struct repo {
			uint64_t id;
			string reponame;
			string userlink;
			uint64_t status; // 0 - init, 1 - pending, 2 - confirmed
			vector<host> hosts; // 3

			auto primary_key() const { return id; }

			EOSLIB_SERIALIZE( repo, (id)(reponame)(userlink)(status)(hosts) )
		};

		typedef eosio::multi_index< N(repos), repo > repos;

		host getnexthost();

	public:
		using contract::contract;

		void addstorage(account_name worker, string ip, string peerid);
		void addrepo(string reponame, string userlink);

		void updatehost(account_name worker, string reponame, string hostlink);
		void validatehost(string reponame, string hostlink, bool valid);
};

EOSIO_ABI( gitpub, (addstorage)(addrepo)(updatehost)(validatehost) )