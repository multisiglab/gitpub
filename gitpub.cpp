#include <gitpub.hpp>
using namespace eosio;

const int HOST_LIMIT = 1;

const uint64_t INIT = 0;
const uint64_t PENDING = 1;
const uint64_t CONFIRMED = 2;

gitpub::host gitpub::getnexthost() {
	storages current_storages(_self, _self);

	int storages_length = distance(current_storages.begin(), current_storages.end());
	eosio_assert(storages_length >= HOST_LIMIT, "too little storages");

	int index = now() % storages_length;
	int index_counter = 0;
	auto current_storage = current_storages.begin();

	while (current_storage != current_storages.end() && distance(current_storages.begin(), current_storage) != index) {
		current_storage++;
	}

	return host(current_storage->worker, current_storage->peerid);
}

void gitpub::addstorage(account_name worker, string ip, string peerid) {
	storages current_storages(_self, _self);

	auto current_storage = current_storages.find(worker);

	if (current_storage == current_storages.end()) {
		// doesn't exist
		current_storages.emplace(_self, [&]( auto& g ) {
			g.worker = worker;
			g.ip = ip;
			g.peerid = peerid;
		});
	} else {
		// exists
		current_storages.modify(current_storage, _self, [&]( auto& g ) {
			g.ip = ip;
			g.peerid = peerid;
		});
	}
}

void gitpub::addrepo(string reponame, string userlink) {
	repos current_repos(_self, _self);

	auto id = N(reponame);

	eosio_assert(current_repos.find(id) == current_repos.end(), "repository with this name already exists");

	current_repos.emplace(_self, [&]( auto& g ) {
		g.id = id;
		g.reponame = reponame;
		g.userlink = userlink;
		g.status = INIT;
		g.hosts = vector<host>();
		for (int i = 0; i < HOST_LIMIT; i++) {
			g.hosts.push_back(getnexthost());
		}
	});
}

void gitpub::updatehost(account_name worker, string reponame, string hostlink) {
	require_auth(worker);

	repos current_repos(_self, _self);

	auto id = N(reponame);
	auto current_repo = current_repos.find(id);

	eosio_assert(current_repo != current_repos.end(), "repository with this name does not exist");
	eosio_assert(current_repo->status < CONFIRMED, "repository with this name is already confirmed");
	eosio_assert(current_repo->status < PENDING, "repository with this name is already pending");

	current_repos.modify(current_repo, _self, [&]( auto& g ) {
		int status_counter = 0;

		auto current_host = g.hosts.begin();

		while (current_host != g.hosts.end()) {
			if (current_host->worker == worker) {
				current_host->hostlink = hostlink;
			}

			if (current_host->hostlink != "") {
				status_counter++;
			}

			current_host++;
		}

		if (status_counter >= HOST_LIMIT) {
			g.status = PENDING;
		}
	});
}

void gitpub::validatehost(string reponame, string hostlink, bool valid) {
	repos current_repos(_self, _self);

	auto id = N(reponame);
	auto current_repo = current_repos.find(id);

	eosio_assert(current_repo != current_repos.end(), "repository with this name does not exist");
	eosio_assert(current_repo->status < CONFIRMED, "repository with this name is already confirmed");

	current_repos.modify(current_repo, _self, [&]( auto& g ) {
		int status_counter = 0;

		auto current_host = g.hosts.begin();
		auto host_to_remove = g.hosts.end();

		while (current_host != g.hosts.end()) {
			if (current_host->hostlink == hostlink && !current_host->status) {
				if (valid) {
					current_host->status = valid;
				} else {
					host_to_remove = current_host;
				}
			}

			if (current_host->status) {
				status_counter++;
			}

			current_host++;
		}

		if (host_to_remove != g.hosts.end()) {
			g.hosts.erase(host_to_remove);
			g.status = INIT;
			g.hosts.push_back(getnexthost());
		} else if (status_counter >= HOST_LIMIT) {
			g.status = CONFIRMED;
		}
	});
}